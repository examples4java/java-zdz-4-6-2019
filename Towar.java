/*
 * Klasa ma przechowywać inforamcje dotyczące towaru dostepnego w sklepie. W związku z tym potrzebuje następujących pól:
 * - kategoria - numer kategorii towaru (wartość integer)
 * - nazwa towaru
 * - skrócona nazwa towaru
 * - opis towaru
 * - ilosc towaru sprowadzonego (łacznie, kolejne dostawy mają się sumować)
 * - ilośc towaru wydanego (łącznie, kolejne sprzedaże mają się sumować)
 * - minimum towaru w magazynie
 * - maksimum towaru w magazynie
 * 
 * Do wszystkich powyższych pól należy dotworzyć odpowiednie metody je modyfikujące.
 * 
 * Ponadto należy utworzyć:
 * - metodę wyliczającą aktualny stan magazynowy produktu
 * - metodę zdejmującą stan magazynowy produktów o ile takowy posiadamy (uwzględniamy minimum oraz wymuszenie sprzedaży minimum)
 * - metodę dodająca stan magazynowy produków (o ile mamy miejsce)
 * - metodę wprowadzającą dany towar do magazynu
 * - metodę kasującą dany towar z magazynu
 */
public class Towar {

}
