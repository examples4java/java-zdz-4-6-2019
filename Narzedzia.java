/* 
 * Klasa ma za zadanie ułatwić komunikację wejścia/wyjścia pomiędzy programem a użytkownikiem
 * Dzięki niej, po wykorzystaniu jej w programie, nie musimy powtarzać odwołań do funkcji 
 * System.out.print; Zamiast tego by cokolwiek wyświetlić wywołujemy <nazwa_zmiennej>.print(), np.
 * 
 * Narzedzia n = new Narzedzia();
 * n.print("Witaj w programie");
 * 
 * Dodatkowym atutem jest brak potrzeby tworzenia dodatkowej zmiennej Scanner celem wczytywania danych do programu.
 * Mając już zainicjowaną zmienną typu Narzędzia możemy bez przeszkód wczytać dane do ciągu znakowego (całą linię - readLine())
 * jak i liczbę całkowitą (readInt()). Przykład:
 * 
 * String tekst = n.readLine();
 * int liczba = n.readInt();
 * 
 * Funkcja readLine() jest efektywniejsza niż Scanner::nextLine() - sprawdza i USUWA potencjalne znaki nowej linii, dzięki czemu nie ma
 * możliwości wczytania śmieciowego wyniku. Nie trzeba jej więc dwukrotnie wywoływać w programie by wyczyścić bufor (sama się tym zajmuje)
 * 
 * Funkcja readInt() jest o tyle ciekawym rozwiązaniem, że nie będziemy mieli problemu w przypadku zle pobrnaej wartosci
 * liczbowej - w takim wypadku ZAWSZE zwroci nam najmniejszą możliwą wartość (ujemną). Stanowi to pewne zabezpieczenie
 * przed destabiblizacją programu.
 * 
 * Dodatkowo klasa posiada dodatkową właściwość errorFlag, która zawiera namniejszą możliwość wartość liczbową dostępną na 
 * danej platformie sprzętowej/maszynie wirtualnej Java. Pozwala ona porównać w prosty sposób wynik readInt() z ewentualnym błędem
 * 
 * Co można jeszcze zrobić (TODO):
 * - utworzyć metodę wczytuającą wartości double (oraz float -> można okroić wartość double)
 * - utworzyć metodę wczytującą wartości słowa (do pierwszej spacji)
 * - utworzyć metodę wczytującą ciąg liczbowy (każda spacja oddziela liczbę; pownny być czytane zarówno liczby
 * całkowite jak i zmiennoprzecinkowe)
 * - utworzyć metodę wyświetlającą tekst bezpośrednio w nowej linii
 * - utowrzyć metodę wyświetlającą tekst i dodającą znak nowej linii na końcu tekstu
 * 
 * INFORMACJA: W przypadku funkcji wprowadzających można wykorzystać, jako podstawę, funkcję readLine() (tak jak zostało to zrobione
 * w przypadku readInt())
 * 
 * ZADANIE DODATKOWE:
 * Utworzyć funkcję wczytującą dane z linii i szukające w ciągu określonej wartości (np. double). 
 */

import java.io.IOException;

//import com.sun.tools.javac.code.Attribute.Array;

public class Narzedzia {
	//metody print, dzięki którym możemy wypisać tekst na konsoli
		//stanowią one przykład tzw. przeładowania funkcji - wszystkie funkcje mają taką samą nazwę jednak przyjmują argumenty
		//różnych typów; dzięki temu można wypisać tą samą funkcją zmienne o różnych typach
	public void print(Object s) {
		System.out.print(s);
	}
	
	public void nPrint(Object s) {
		print("\n"+s);
	}
	
	public void printN(Object s) {
		print(s+"\n");
	}
	
	public void error(Object s) {
		System.err.print(s);
	}
	
	public String readLine() {
		String r="";
		int b=-1;
		//13 ==> \n
		try {
			while((b=System.in.read()) != 13) {
				r += (char)b;
			}
		}
		catch (IOException error) {
			error("COŚ POSZŁO NIE TAK! Błąd: " + error.getMessage());
		}
		r = r.replaceAll("\n", "");
		return r;
	}
	
	public String read() {
		try {
			return readLine().split(" ")[0];
		}
		catch (Exception e) {}
		return "";
	}
	
	public double readDouble() {
		try {
			return Double.valueOf(readLine());
		}
		catch (NumberFormatException e) {
			error("Nastąpił błąd zamiany na liczbę!");
		}
		return Integer.MAX_VALUE*-1-1;
	}
	
	public float readFloat() {
		return (float)readDouble();
	}
	
	public int readInt() {
		return (int)readDouble();
	}
	
	public boolean readBool() {
		return (readInt() != 0) ? true : false;
	}
	
	public double[] readNumbers() {
		String[] ss = readLine().split(" ");
		double[] r = new double[ss.length];
		int i=0;
		for (String s : ss) {
			try {
				r[i++]=Double.valueOf(s.replace(",", "."));
			}
			catch (NumberFormatException e) {}
		}
		return r;
	}
	
	public Object[] readFind(Typy type, String delimiter) {
		return readFind(type.get(), delimiter);
	}
	
	public Object[] readFind(int type, String delimiter) {
		
		String[] ss = readLine().split(delimiter);
		//tworzymy prostą tablicę do zwrotu; tablica będzie się powiększać
		//za każdym razem gdy będziemy do niej dodawać kolejne elementy 
		//(przykładowo znajdziemy wartość double podczas szukania double
		//to wartość powędruje do tablicy zwięszkając jej rozmiar o jeden)
		Object[] r = null;
		switch (type) {
		case 0:
			for (String v:ss) {
				try {
					r = addToArray(r, Integer.valueOf(v.replace(",", ".")));
				}
				catch (NumberFormatException e) {}
			}
			break;
		case 1:
			for (String v:ss) {
				try {
					r = addToArray(r, Double.valueOf(v.replace(",", ".")));
					//r[i++]=Double.valueOf(v.replace(",", "."));
				}
				catch (NumberFormatException e) {}
			}
			break;
		case 2:
			for (String v:ss) {
				try {
					r = addToArray(r,Boolean.valueOf(v.replace(",", ".")));
				}
				catch (NumberFormatException e) {}
			}
			break;
		case 3:
			for (String v:ss) {
				try {
					Double.valueOf(v.replace(",", "."));					
				}
				catch (NumberFormatException e) {
					r = addToArray(r, v);
				}
			}
		default:
			break;
		}
		
		return r;
	}
	//funkcja kopiuje podaną tablicę do nowej tablicy,o jeden większej,
	//po czym dodaje na ostatniej pozycji nowy element 
	// - a - tablica wejściowa (do powiększenia)
	// - e - element do dodania na końcu
	// zwraca nową tablicę, w której znajduje się nowy element
	private Object[] addToArray(Object[] a, Object e) {
		Object[] r = new Object[(a!=null) ? a.length+1 : 1];
		for(int i=0;i<((a!=null) ? a.length: 0);i++)r[i]=a[i];
		r[r.length-1] = e;
		return r;
	}
	//na podstawie powyższej funkcji warto dorobić:
	// - dodawanie przed obecnymi wartościami (tzw. prepend)
	// - dodanie za wskazanym elementem tablicy
	// - usuwanie z tablicy wskazanego elementu (zmniejszając tym samym jej rozmiar)
	
	public enum Typy {
		INTEGER(0),
		DOUBLE(1),
		BOOLEAN(2),
		STRING(3);
		
		private final int v;
		
		private Typy(int v) {this.v=v;}
		
		//public int get() {return this.v;}
		public int get() {return v;}
	}
	
}