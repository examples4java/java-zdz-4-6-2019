/*
 * Klasa mająca na celu wskazanie jak działa obiektowość. Plik ten można podłączyć do dowolnego pgoramu w języku Java - 
 * wystarczy umieścić go w zródłach i/lub podłączyć do projektu. Każdy plik klasy MUSI posiadać przynajmniej jedną publiczną klasę (inne 
 * możliwe modyfikatory poznane zostaną pózniej). 
 * 
 * Klasy to próba odwzorowania naszego, analogowego świata, w cyfrowym świecie komputerowym. My, jako ludzie, postrzegamy wszystko jako 
 * proste/złożone przedmioty. Wszystko, z czym się na co dzień stykamy, ma znamiona obiektowości - szklanka (wykonana z określonego tworzywa,
 * określona trwałość, pojemność, kształt czy kolor), meble (określony kolor/deseń, twardość, kształt, typ, zastosowanie) czy też my sami 
 * (określony kolor włosów, oczu, płeć, rasa, karnacja itp.). Chociaż wymienione powyżej cechy każdy z obiektów posiada inne, to jednak każdy z nich
 * je posiada (a przynajmniej rzadko zdarza się brak którejś z cech, chociaż to też jej stan). Dodatkowo trzeba pamiętać, że każdy z przedmiotów ma
 * tzw. swoje funkcje i funkcjonalności, jak chodzenie, podnoszenie czy czytanie (ludzie), dzwoenienie, wyświetlanie, nawigowanie (telefony), wytwarzanie
 * tlenu, wilgoci czy ochrona przed słońcem (drzewa).
 * Klasa to nic innego jak DEFINICJA (nadanie pewnych cech, pewnych zachowań względem określonych czynników) zachowania naszego własengo typu w programowaniu.
 * Klasa jest więc, w dużym uproszczeniu, tym samym co int, float czy double. Jednak wymienione tutaj nie mają swoich cech, właściwości czy większych interakcji - 
 * robią dokładnie jedną rzecz przechowując jeden, określony rodzaj danych (wszystkie wymienione przechowują typ liczbowy). Klasy zaś mogą przechowywać 
 * w sobie dowolną (w założeniu) ilość typów prostych czy złożonych. To od nas zależy ile potencjalnych zmiennych (w klasach zwalnych polami) będzie znajdować
 * się w naszym typie. 
 * 
 * Dodatkowo klasy mogą posiadać funkcje (tutaj zwane metodami). Metody pozwalają na wykonywanie pewnych zadań charakterystycznych dla utworzonego przez nas
 * typu. Jeżeli przykładowo utworzymy klase Mysz, nadamy jej funkcje lewyKlik(), prawyKlik(), przesuniecie(), srodkowyKlik(). Wszystko to zakładając, że 
 * mamy do czynienia z myszką podstawową, bez makr. każda z tych funkcji powinna dawać takie rezultaty, jak fizyczny sprzęt w przypadku klikania na odpowiednie
 * przyciski. 
 * 
 * Więcej na temat klas:
 * https://www.tutorialspoint.com/java/java_object_classes.htm
 * https://www.geeksforgeeks.org/classes-objects-java/
 * https://www.w3schools.com/java/java_classes.asp
 */

/*
 * NALEŻY DODAĆ!:
 * - dodac typ wyliczeniowy dla uperwanień:
 * a) Administrator
 * b) Pracownik
 * c) Magazynier
 * d) Klient
 */

public class Osoba {
	/*
	 * Poniżej przykład klasy utworzonej wewnątrz klasy nadrzędnej (Osoba). W tym wypadku klasa ta ma zasięg prywatny - jest w pełni dostępna w obrębie
	 * pliku klasy Osoba, jednak poza nią jest ona niewidoczna. Tylko metody wewnątrz klasy Osoba będą mogły wchodzić w interakcję z Miejscowosc. Jest to o
	 * tyle wygodne, że to klasa Osoba będzie zarządzać ewentualnymi interkacjami z Miejscowosc.	
	 */
	
	private class Miejscowosc {
		private String kod,nazwa,dzielnica;
		//przykład konstruktora prywatnego. Tego typu konstruktor może być wywoływany jedynie przez metody klasy.
		//w poniższym wypadku konstruktor może być też wywołwany przez klasę nadrzędną.
		private Miejscowosc() {
			kod=nazwa=dzielnica=null;
		}
		
		private Miejscowosc(String kod, String nazwa) {
			this.kod=kod;
			this.nazwa=nazwa;
		}
		
		private Miejscowosc(String kod, String nazwa, String dzielnica) {
			this.kod=kod;
			this.nazwa=nazwa;
			this.dzielnica=dzielnica;
		}
		
		public boolean setKod(String kod) {
			//sprawdz czy podany ciag ma mniej niż 5 znaków lub ma więcej
			//niż 6 znaków; jeżeli tak - jest błędny i należy zakończyć
			//jego wprowadzanie
			if (kod.length() < 5 || kod.length() > 6) return false;
			//wyczyść dotychczasowy kod
			this.kod="";
			//jeżeli kod ma 5 znaków znaczy, że ktoś nie dodał myślnika;
			//zrobimy to za niego
			if (kod.length()==5) {
				//przesuwamy się po każdym elemencie kodu znak po znaku
				for(int i = 0;i<5;i++) {
					//jeżeli jesteśmy na 3 pozycji (liczymy od zera)
					//to dodajemy myślnik
					if(i==2) 
						this.kod+="-";
					//niezależnie od warunku powyżej kontynuejmy
					//przepisywanie naszego kodu do zmiennej (pola)
					//klasy Miejscowosc
					this.kod+=kod.charAt(i);
				}
			}
			else
				this.kod=kod;
			return true;
		}
		
		public void setNazwa(String nazwa) {
			this.nazwa=nazwa;
		}
		
		public void setDzielnica(String dzielnica) {
			this.dzielnica = dzielnica;
		}
		
		@SuppressWarnings("unused")
		public String getKod() {
			return kod;
		}
		
		@SuppressWarnings("unused")
		public String getNazwa() {
			return nazwa;
		}
		
		@SuppressWarnings("unused")
		public String getDzielnica() {
			if (dzielnica==null) return "";
			return dzielnica;
		}
		
		//metody nie muszą zwracać pojedynczych wartości; mogą zwracać także połączenie wartości, odpowiednie wartości względem
		//zawartości zmiennych itp.
		public String getMiejscowosc() {
			//np. 42-200, Częstochowa, dzienica zawodzie			
			return kod + ", " + nazwa +
					((dzielnica!=null) ? ", dzielnica " + dzielnica : "");
		}
	}
	
	//przykład klasy, która dziedziczy (w Java dosłownie rozszerza) klasę Miejscowosc; dzięki temu klasa Adres, prócz swoich pól
	//posiada wszystkie składowe (pola+metody) o zasięgu publicznym i chronionym klasy Miejscowosc. Dzięki temu to ona (klasa Adres)
	//może zarządzać wszystkimi ustawieniami klasy Miejscowosc (a tamta nie musi być przez nikogo wywoływana; i nawet nie może). Klasa
	//Adres ma prawo do wywoływania konstruktorów prywatnych klasy bazowej (po której dziedziczy). 
	
	//UWAGA! W języku Java klasa może dziedziczyć TYLKO PO JEDNEJ KLASIE!
	private class Adres extends Miejscowosc {
		private String ulica,posesja,lokal;
		private int typ;
		//konstruktor numer 1 - nadpisanie domyślnego (bez parametrów wejściowych)
		//należy zauważyć, że wywołuje on konstruktor klasy Miejscowosc() (by mieć dostęp do jej składowych)
		public Adres() {
			typ = 3;
			lokal=null;
			new Miejscowosc();
		}
		//przeciążenie konstruktora; działa gdy programista poda ulicę oraz numer posesji
		@SuppressWarnings("unused")
		public Adres(String ulica, String posesja) {
			new Adres();
			this.ulica=ulica;
			this.posesja=posesja;
		}
		//jw. lecz można podać także numer lokalu
		@SuppressWarnings("unused")
		public Adres(String ulica, String posesja, String lokal) {
			new Adres();
			this.ulica=ulica;
			this.posesja=posesja;
			this.lokal=lokal;
		}
		//ten konstruktor przyjmuje w zasadzie wszystkie parametry do ustawinia w nowym obiekcie; 
		//sam jednak nie tworzy obiektu lecz odwołuje się do drugiego konstruktora (przeciążonego) 
		//który zamiast posiadać parametr o typie Droga ma parametr typu int 
		@SuppressWarnings("unused")
		public Adres(Droga typ, String ulica, String posesja, String lokal, String kod, String miejscowosc) {
			new Adres(typ.get(),ulica,posesja,lokal,kod,miejscowosc);
		}
		
		public Adres(int typ, String ulica, String posesja, String lokal, String kod, String miejscowosc) {
			this.ulica=ulica;
			this.posesja=posesja;
			this.lokal=lokal;
			this.typ = typ;
			new Miejscowosc(kod, miejscowosc);
		}
		
		
		@SuppressWarnings("unused")
		public void setUlica(String ulica) {this.ulica=ulica;}
		
		@SuppressWarnings("unused")
		public void setPosesja(String posesja) {this.posesja=posesja;}
		
		@SuppressWarnings("unused")
		public void setLokal(String lokal) {this.lokal=lokal;}
		
		@SuppressWarnings("unused")
		public void setTypDrogi(int typ) {this.typ=typ;}
		
		@SuppressWarnings("unused")
		public void setTypDrogi(Droga typ) {this.typ=typ.get();}
		
		public String getAdres() {
			return ((typ==Droga.PLAC.get()) ? "pl. " : 
				((typ==Droga.ALEJA.get()) ? "al. " : ((typ==Droga.ULICA.get()) ? "ul. " : ""))) + ulica + 
				" " + posesja + (lokal!=null ? " mieszkania " + lokal : "") + " " + getMiejscowosc();
		}
	}
	//typ wyliczeniowy; pozwala wykorzystywać nazwy zamiast np. wartości liczbowe. Zaletą stosowania typu wyliczeniowego jest mniejszy
	//rozmiar zmiennej (jedna litera = co najmniej 1 bajt danych); nazwy w typie enum rezydują w pamięci programu, w pamięci RAM zajmując tyle ile wynosi 
	//ich wartość podana w nawiasie (w naszym wypadku jest to integer - 4 bajty). 
	public enum Droga {
		//wymienione po przecinku wszystkie możliwe stany typu enum; w nawiasie podane są jego odpowiedniki licznowe (mogą byc to dowolne,
		//inne typy)
		ULICA(0),
		PLAC(1),
		ALEJA(2),
		BRAK(3);
		//zmienna przechowująca prawdziwą wartość zmienne enum (podaną wyżej w nawiasie)
		private final int wartosc;
		//prywatny konstruktor nadający wartość zmiennej typy enum. 
		//w tym wypadku konstruktor MUSI być prywatny (wywołuje się niejawnie wraz z wywołaniem jednego ze stanu enum)
		private Droga(int wartosc) {this.wartosc=wartosc;}
		//jedyna publiczna funkcja; pozwala na wyciągnięcie wartości zapisanej pod naszą zmienną typu enum
		public int get() {return wartosc;}
	}
	
	//poniżej jest przykład konstruktora klasy; konstruktory to metody (funkcje), jednak nie posiadają one standardowego
	//zwrócenia wartości (takiego jak void, int czy float). Konstruktory pozwalają na ustawienia początkowe niektórych
    //własności nowego obiektu tworzonego na podstawie klasy (powinny w zasadzie ustawiać wszystkie własności). 
	//Jeżeli sami nie zdefiniujemy konstrutora, zostanie on stworzony niejawnie (bez naszej wiedzy i "zgody"). Taki domyślny konstruktor
	//(tak się go nazywa) będzie zerował wszystkie zmienne (lub ustawiał je na wartość null). Poniższy konstrutor robi doładnie 
	//to co robiłby konstruktor domyślny - tutaj jednak "sami" o tym decydujemy
	public Osoba() {
		imie=nazwisko=pesel=null;
		uprawnienia=0;
	} 
	
	public boolean setImie(String i) {
		if (i.length()<2) return false;
		if (i.charAt(0) < 'A' || i.charAt(0) > 'Z') return false;
		imie = i;
		return true;
	}
	
	public String getImie() {
		return imie;
	}
	
	public boolean setNazwisko(String i) {
		if (i.length()<2) return false;
		if (i.charAt(0) < 'A' || i.charAt(0) > 'Z') return false;
		nazwisko = i;
		return true;
	}
	
	public String getNazwisko() {
		return nazwisko;
	}
	//ustawianie numeru PESEL wraz ze sprawdzeniem czy jest on poprawny (w komentarzu podany wzór na sprawdzanie)
	public boolean setPESEL(String p) {
		if (p.length() != 11) return false;
	    if (Integer.valueOf(String.valueOf(String.valueOf(p.charAt(4)) + p.charAt(5))) > 31) return false; 
	    if (String.valueOf(String.valueOf(p.charAt(0)) + p.charAt(1)) == "88") return false; 
	    if (String.valueOf(String.valueOf(p.charAt(2)) + p.charAt(2)) == "88") return false; 
	    if (String.valueOf(String.valueOf(p.charAt(3)) + p.charAt(4)) == "88") return false; 
	  //1×a + 3×b + 7×c + 9×d + 1×e + 3×f + 7×g + 9×h + 1×i + 3×j + 1×k
	    int c = (Integer.valueOf(p.charAt(0)) + 3*Integer.valueOf(p.charAt(1))+ 7*Integer.valueOf(p.charAt(2)) +
	    		9*Integer.valueOf(p.charAt(3)) + Integer.valueOf(p.charAt(4)) + 3*Integer.valueOf(p.charAt(5)) +
	    		7*Integer.valueOf(p.charAt(6))+9*Integer.valueOf(p.charAt(7))+Integer.valueOf(p.charAt(8))+
	    		3*Integer.valueOf(p.charAt(9))+Integer.valueOf(p.charAt(10))) % 10;
	    if (c != 0) return false;
	    pesel=p;		    		
	    return true;
	}
	
	public void createAdres() {
		adres = new Adres();
		
	}
	//kolejny przykład przeciążonej funkcji; tym razem funkcja, w zalezności od parametrów
	//albo wywołuje sama siebie (ze zmienionymi parametrami) albo tworzy nowy obiekt klasy Adres
	
	/*
	 * Obiekt to DEKLARACJA zmiennej typu klasa. Obiekt, w przeciwieństwie do klasy, REZYDUJE (znajduje się) w pamięci RAM
	 * Klasa natomiast znajduje się w pamięci programu i nie znajduje się w pamięci RAM. Ze składowych zdefiniowanych w klasie
	 * możemy zacząć korzystać dopiero wtedy gdy stworzymy zmienną-obiekt!
	 */
	public void createAdres(String ulica, String numer) {
		createAdres(Droga.BRAK, ulica, numer);
	}	
	
	public void createAdres(Droga typ, String ulica, String numer) {
		createAdres(typ.get(),ulica,numer);
	}		
	
	public void createAdres(int typ, String ulica, String numer) {
		adres = new Adres(typ, ulica, numer, null, null, null);
	}
	
	public void createAdres(Droga typ, String ulica, String numer, String lokal) {
		createAdres(typ.get(),ulica,numer,lokal,null,null);
	}
	
	public void createAdres(int typ, String ulica, String numer, String lokal, String kod, String miejscowosc) {
		adres = new Adres(typ, ulica, numer, lokal, kod, miejscowosc);
	}
	
	public void setMiejscowosc(String nazwa, String kod) {
		if(adres==null) return;
		adres.setNazwa(nazwa);
		adres.setKod(kod);
	}
	
	public void setMiejscowosc(String nazwa, String kod, String dzielnica) {
		if(adres==null) return;
		adres.setNazwa(nazwa);
		adres.setKod(kod);
		adres.setDzielnica(dzielnica);
	}
	
	/*
	 * minimum 9 znaków
	 * jeżeli 9 znaków - na początek wrzucamy +48
	 * dodajemy spacje/myślniki jeżeli jest ich brak
	 */
	
	public boolean setTelefon(String telefon) {
		if(telefon.length()<9 || telefon.length()>17) return false;
		telefon = telefon.replace("(", "");
		telefon = telefon.replace(")", "");
		telefon = telefon.replace(" ", "");
		telefon = telefon.replace("-", "");
		for(int i=0;i<telefon.length();i++) {
			if ((telefon.charAt(i) > '9' || telefon.charAt(i)<'0') && 
					(telefon.charAt(i)!= '+' && telefon.charAt(i)!='#')) {
				return false;
			}
		}
		if (telefon.length()==9) telefon = "+48"+telefon;
		this.telefon="";
		for(int i=3;i<=telefon.length();i+=3)
			this.telefon+=telefon.substring(i-3,i)+"-";
		this.telefon=this.telefon.substring(0,this.telefon.length()-1);
		return true;
	}
	
	public String getTelefon() {
		return telefon;
	}
	
	public String getAdres() {
		return adres.getAdres();
	}
	
	public String getPESEL() {
		return pesel;
	}
	
	public boolean setUprawnienia(int u) {
		if (u<0 || u>2) return false;
		uprawnienia=u;
		return true;
	}
	
	public int getUprawnienia() {
		return uprawnienia;
	}

	private String imie,nazwisko;
	private String pesel;
	private int uprawnienia;
	private Adres adres;
	private String telefon;
}