/*
 * Klasa mająca spinać wszystkie pozostałe
 * Poszczególne klasy:
 * - osoba
 * - pracownik
 * - dostawy
 * - towary
 * - magazyn
 * 
 * mają tutaj zostać połączone jako całość. Dobrym pomysłem będzie złączyć ze sobą kilka klas
 * (np. pracownik jako rozszerzenie klasy Osoba) czy też niektóre klasy z osoby wykorzystać do innych klas
 * (np. Adres, który może być przydatny zarówno w osobach jak i magazynie). Należy wypróbować prgram  
 * (dobrze będzie przetestować działanie każdej stworzonej klasy osobno, dopiero pózniej przetestować je łącznie)
 */
public class Main {

}
